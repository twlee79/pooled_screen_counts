#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Parse GeCKO v2 Human Genome A+B library contents file, reporting any sgRNAs
with multiple matches (many), producing a fasta file of sgRNAs labelled with
"gene_id___index_sequence", where gene_id is "gene_id" column from contents
file, index is the numeric suffix from the "UID" column with 100000 added to
this number for HGLibB (B library) sgRNAs, and sequence is the sgRNA sequence.
Multiple matches are defined as identical sgRNAs that match >1 different
gene_id. For these, the gene_id is replaced with "_multipleX" where X is a
number indicating the number of different gene_ids that were given for this
sgRNA, and first index found is used. Where an sgRNA is provided twice for
the same gene_id, its index is set to the index of first sgRNA found.

To process only one half of the library, provide the desired half library
with the '-a' option and have

Expected contents files:
human_geckov2_library_a_09mar2015.csv
human_geckov2_library_b_09mar2015.csv
From:
https://media.addgene.org/cms/filer_public/a4/b8/a4b8d181-c489-4dd7-823a-fe267fd7b277/human_geckov2_library_a_09mar2015.csv
https://media.addgene.org/cms/filer_public/2d/8b/2d8baa42-f5c8-4b63-9c6c-bd98f333b29e/human_geckov2_library_b_09mar2015.csv
"""
import csv
import argparse
import sys
from collections import OrderedDict


_PROGRAM_NAME = 'parse_gecko_hg'
_PROGRAM_VERSION = '1.0.2'
# -------------------------------------------------------------------------------
# Author:       Tet Woo Lee
#
# Created:      2019-01-14
# Copyright:    (c) Tet Woo Lee 2019
# Licence:      GPLv3
#
# Dependencies: None
#
# Change log:
# 1.0.2 2019-06-26: Better statistics on number of multiple matches
# 1.0.1 2019-02-12: Added sequence to end of identifier
# -------------------------------------------------------------------------------

parser = argparse.ArgumentParser(prog=_PROGRAM_NAME,
                                 description=__doc__,
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--version', action='version',
                    version='{} {}'.format(_PROGRAM_NAME, _PROGRAM_VERSION))
parser.add_argument('-a','--inputfile-a', default="human_geckov2_library_a_09mar2015.csv",
                    help='Input Gecko A library contents file')
parser.add_argument('-b','--inputfile-b', default="human_geckov2_library_b_09mar2015.csv",
                    nargs='?', const=None,
                    help='Input Gecko B library contents file (optional)')
parser.add_argument('-o','--outputfile',
                    help='Output fasta file, otherwise stdout used')

args = parser.parse_args()
input_filename_a = args.inputfile_a
input_filename_b = args.inputfile_b
print(input_filename_a ,input_filename_b)
output_filename = args.outputfile

input_filenames = [input_filename_a]
if input_filename_b is not None:
    input_filenames.append(input_filename_b)

sgrna_list = []

for library_number, input_filename in enumerate(input_filenames):
    with open(input_filename,"r") as input_handle:
        reader = csv.DictReader(input_handle,delimiter=",")
        for index,row in enumerate(reader):
            sgrna = row
            sequence = sgrna["seq"]
            gene = sgrna["gene_id"]
            library,index = sgrna["UID"].split("_")
            sgrna["library"] = library
            index = int(index)
            if library_number==1: index += 100000 # processing B library
            sgrna["index"] = index
            sgrna["name"] = "{}___{}_{}".format(gene,index,sequence)
            sgrna_list.append(sgrna)

sgrna_seq_lookup = OrderedDict()

# find sgrnas with same sequence
for sgrna in sgrna_list:
    matching_set = sgrna_seq_lookup.setdefault(sgrna["seq"],list())
    matching_set.append(sgrna)

# process list of multiple sgrnas
unique_sgrna_list = []
print("List of multiple matches:")
num_multiple_gene_id_seqs = 0 # number of unique sequences with that match multiple gene_ids
num_multiple_gene_id_gene_ids = 0 # number of different gene_ids that share sequences with other gene_ids
num_multiple_gene_id_items = 0 # number of original sgrna items that actual match sequences of other gene_ids
num_dereplicated_seqs = 0 # number of unique sequences for same gene_id (incl NTCs) de-replicated
num_dereplicated_items = 0 # number of original sgrna items de-replicated
for sgrna_seq,matching_set in sgrna_seq_lookup.items():
    if len(matching_set)>1: # >1 sgrna, same sequence, check is it different gene_id?
        unique_gene_ids = OrderedDict()
        for sgrna in matching_set:
            matches_this_gene_id = unique_gene_ids.setdefault(sgrna["gene_id"], list())
            matches_this_gene_id.append(sgrna)
        if len(unique_gene_ids)>1:
            list_of_gene_id_and_index_tuples = list((gene_id,[sgrna["index"] for sgrna in all_sgrnas]) for gene_id,all_sgrnas in unique_gene_ids.items())
                # generates a list of [(gene_id1, [indexes]), (gene_id2, [indexes]),...)
            num_multiple_gene_ids = len(list_of_gene_id_and_index_tuples)
                # number of different gene_ids
            num_multiple_items = sum(len(indexes) for gene_id,indexes in list_of_gene_id_and_index_tuples)
                # number of different items; some gene_ids may have multiple items with identical sequence also matching different gene_ids
            the_gene_id = "_multiple{}".format(num_multiple_gene_ids)
                # _multipleX where X is number of different gene_ids matched by this sequence
            the_index = list_of_gene_id_and_index_tuples[0][1][0] # first gene_id, second element in tuple (indexes), and first index
            print("{} matches gene_ids {}, indexes {}, replacing as {}___{}_{}".format(sgrna_seq,
                ", ".join(item[0] for item in list_of_gene_id_and_index_tuples),
                "; ".join(str(item[1]) for item in list_of_gene_id_and_index_tuples),
                the_gene_id,the_index,sgrna_seq))
            num_multiple_gene_id_seqs += 1
            num_multiple_gene_id_gene_ids += num_multiple_gene_ids
            num_multiple_gene_id_items += num_multiple_items
        else:
            the_gene_id,all_sgrnas = next(iter(unique_gene_ids.items())) # get first and only value
            the_index = all_sgrnas[0]["index"] # first index
            num_dereplicated_seqs += 1
            num_dereplicated_items +=  len(all_sgrnas)
            print("{}/{} has multiple indexes {}; using first {}".format(sgrna_seq,
                the_gene_id,
                list(sgrna["index"] for sgrna in all_sgrnas ),
                the_index))
        # add sgrna with fixed name to unique sgrna list
        new_name = "{}___{}_{}".format(the_gene_id,the_index,sgrna_seq)
        sgrna = {}
        sgrna["seq"] = sgrna_seq
        sgrna["name"] = new_name
        unique_sgrna_list.append(sgrna)
    else:
        unique_sgrna_list.append(matching_set[0]) # only single match, use directly
print("Number de-replicated (same gene_id, identical sequence): {} items, {} unique sequences".format(num_dereplicated_items, num_dereplicated_seqs))
print("Number with multiple matches (different gene_id, identical sequence): {} items, {} gene_ids, {} unique sequences".format(num_multiple_gene_id_items, num_multiple_gene_id_gene_ids,num_multiple_gene_id_seqs))

print("Writing {} sgRNA sequences as fasta.".format(len(unique_sgrna_list)))
handle = open(output_filename, 'w') if output_filename else sys.stdout
for sgrna in unique_sgrna_list:
  handle.write(">{}\n{}\n".format(sgrna["name"],sgrna["seq"]))

if handle is not sys.stdout:
    handle.close()
