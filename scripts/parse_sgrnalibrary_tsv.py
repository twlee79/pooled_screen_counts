#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Parse generic library contents file to produe a fasta file of sgRNAs 
labelled with "Target-Gene-Symbol___index_sequence", where Target-Gene-Symbol 
is "Target Gene Symbol" column from contents file with spaces replaced with
dashes (e.g. Non Targeting Controls), sequence is sgRNA sequence from
selected column and index is value from appropriate column (if selected)
or an increasing index from 1.

Note that sgRNA sequences represented multiple times (i.e replicated 
sequences) are reported but not otherwise processed differently. Completely
duplicated entries (same label and sequence) will cause an exception.

Default options work with: broadgpp-brunello-library-contents.txt
From: https://www.addgene.org/static/cms/filer_public/8b/4c/8b4c89d9-eac1-44b2-bb2f-8fea95672705/broadgpp-brunello-library-contents.txt
"""
import csv
import argparse
import sys
from collections import OrderedDict


_PROGRAM_NAME = 'parse_sgrnalibrary_tsv'
_PROGRAM_VERSION = '1.0.2'
# -------------------------------------------------------------------------------
# Author:       Tet Woo Lee
#
# Created:      2019-01-02
# Copyright:    (c) Tet Woo Lee 2019
# Licence:      GPLv3
#
# Dependencies: None
#
# Change log:
# 1.0.2 2019-05-23: Converted to generic form, originally parse_brunello.py
# 1.0.1 2019-02-12: Added sequence to end of identifier
# -------------------------------------------------------------------------------

parser = argparse.ArgumentParser(prog=_PROGRAM_NAME,
                                 description=__doc__,
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--version', action='version',
                    version='{} {}'.format(_PROGRAM_NAME, _PROGRAM_VERSION))
parser.add_argument('-i','--inputfile', default="broadgpp-brunello-library-contents.txt",
                    help='Input library contents file, as tab-separated values')
parser.add_argument('-g','--genesymbolcolumn', default="Target Gene Symbol",
                    help='Column heading to read Target-Gene-Symbol')
parser.add_argument('-s','--sequencecolumn', default="sgRNA Target Sequence",
                    help='Column heading to read sgRNA sequence')
parser.add_argument('-n','--indexcolumn',
                    help='Column heading to read sgRNA index, if absent sequential index used')
parser.add_argument('-o','--outputfile',
                    help='Output fasta file, otherwise stdout used')

args = parser.parse_args()
input_filename = args.inputfile
output_filename = args.outputfile
genesymbol_columnname = args.genesymbolcolumn
sequence_columnname = args.sequencecolumn
index_columnname = args.indexcolumn

sgrna_list = []

with open(input_filename,"r") as input_handle:
    reader = csv.DictReader(input_handle,delimiter="\t")
    if genesymbol_columnname not in reader.fieldnames:
      raise Exception("Could not find gene symbol column {} in file".format(genesymbol_columnname))
    if sequence_columnname not in reader.fieldnames:
      raise Exception("Could not find sequence column {} in file".format(sequence_columnname))
    if index_columnname is None:
      print("No index column specified, using a sequential index")
    elif index_columnname not in reader.fieldnames:
      raise Exception("Could not find index column {} in file".format(index_columnname))
    for index,row in enumerate(reader):
        sgrna = row
        sequence = sgrna[sequence_columnname]
        sgrna["seq"] = sequence
        gene = sgrna[genesymbol_columnname].replace(" ","-")
        if index_columnname is None:
          indexout = index+1
        else:
          indexout = sgrna[index_columnname]
        sgrna["name"] = "{}___{}_{}".format(gene,indexout,sequence)
        sgrna_list.append(sgrna)

sgrna_seq_lookup = OrderedDict()

for sgrna in sgrna_list:
    matching_set = sgrna_seq_lookup.setdefault(sgrna["seq"],OrderedDict())
    if sgrna['name'] in matching_set:
      raise Exception("Completely duplicated (label and sequence) sgRNA found: {}".format(sgrna['name']))
    matching_set[sgrna['name']] = True

print("List of multiple matches:")
num_multiple = 0
for sgrna_seq,matching_set in sgrna_seq_lookup.items():
    if len(matching_set)>1:
        print("{} matches {}".format(sgrna_seq, ", ".join(matching_set)))
        num_multiple+=1
print("Number of multiple matches: {}".format(num_multiple))

print("Writing {} sgRNA sequences as fasta.".format(len(sgrna_seq_lookup)))
handle = open(output_filename, 'w') if output_filename else sys.stdout
for sgrna in sgrna_list:
  handle.write(">{}\n{}\n".format(sgrna["name"],sgrna["seq"]))

if handle is not sys.stdout:
    handle.close()
