#!/bin/sh
# -*- coding: utf-8 -*-
# download file
wget --directory-prefix=input/libraries/ --timestamping https://www.addgene.org/static/cms/filer_public/8b/4c/8b4c89d9-eac1-44b2-bb2f-8fea95672705/broadgpp-brunello-library-contents.txt
# further processing handled by Snakemake workflow
