#!/bin/sh
# -*- coding: utf-8 -*-
# download files
wget --directory-prefix=input/libraries/ --timestamping https://media.addgene.org/cms/filer_public/a4/b8/a4b8d181-c489-4dd7-823a-fe267fd7b277/human_geckov2_library_a_09mar2015.csv
wget --directory-prefix=input/libraries/ --timestamping https://media.addgene.org/cms/filer_public/2d/8b/2d8baa42-f5c8-4b63-9c6c-bd98f333b29e/human_geckov2_library_b_09mar2015.csv
# process with python script
python scripts/parse_geckov2hg.py -a input/libraries/human_geckov2_library_a_09mar2015.csv -b input/libraries/human_geckov2_library_b_09mar2015.csv -o input/libraries/GeCKOv2HG_nomultiple_gene_id___index.fasta
# add PCR controls sequences
cat input/libraries/GeCKOv2HG_nomultiple_gene_id___index.fasta input/libraries/PCRcontrols_renamed.fasta >input/libraries/GeCKOv2HG_nomultiple-PCRctrls_gene_id___index.fasta
