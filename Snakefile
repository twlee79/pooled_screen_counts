configfile: "prepare_input/prepare_input_default_config.yaml" # defaults for prepare_input.smk
configfile: "default_config.yaml"  # default configuration parameters
configfile: "override_config.yaml" # overrides any default configuration parameters

# override print in prepare_input, so that we don't print until barcode mappings gone
def print_input_sample_details():
    pass

include: "prepare_input/prepare_input.smk" # use prepare_input generic workflow to read sample_list and get list of input files
print_input_sample_details = print_input_sample_details_default # reset print function


# add suffixes and presets to templater objects
for templater in (path_templater,input_source_templater):
    templater.add_alt_suffixes(config['path_templater_additional_extensions'])
    templater.add_preset_formats({'fastqc_file': {'logdir': ([],{}), 'new_affix' : (["_fastqc"],{}), 'new_suffix':([".html"],{}) },
                                  'all_inputs': {'apply_format': ([],{'inputid':"all"}) }
                                 })

# simple templaters for templating without sample_names etc.
basic_templater = path_templater.create("{directory}/{filename}")
    # simple directory/filename templater
    # defaults to intermediate dir

toptemplate_basic_templater = basic_templater.toptemplatedir()
    # alternative basic templater that uses toptemplatedir
    # i.e. top directory as template {topdirectory}
    # allows it to work in any top directory
    # side effect is can't be used in all rules, is {topdirectory} isn't
    # used for log files, potentially giving ambiguous outputs

basic_fastq_gz_templater = basic_templater.fastq_gzfile()
basic_fastqc_templater = basic_templater.fastqc_file()
basic_bam_templater = basic_templater.bamfile()
basic_sam_templater = basic_templater.samfile()
basic_fasta_templater = basic_templater.fastafile()

toptemplate_basic_fasta_templater = toptemplate_basic_templater.fastafile()

# f/r barcode parameters to used internally, determine from config file
f_barcode_parameter_to_use = "barcode_" + config["f_barcode_parameter_to_use"]
r_barcode_parameter_to_use = "barcode_" + config["r_barcode_parameter_to_use"]

# lookup dict of barcode parameters from barcode labels
barcode_parameter_from_label = {}

# lookup barcode parameter and barcode label from config file
# barcode_parameter is the barcode sequence/adapter etc. used by scripts when processing data
# barcode_label is the label used for the barcode when labelling files processed for this barcode
def lookup_barcode_from_config(barcode, barcode_parameter_to_use):
    if barcode_parameter_to_use not in config:
        raise ValueError("Barcode parameter type {} not found in config file".format(
            barcode_parameter_to_use))
    if barcode in config[barcode_parameter_to_use]:
        barcode_parameter = config[barcode_parameter_to_use][barcode]
        print("From config file: Barcode {} using parameter {} -> {}.".format(barcode,
                                                                              barcode_parameter_to_use, barcode_parameter))
    else:
        raise ValueError("Barcode {} parameter {} not found in config file".format(
            barcode, barcode_parameter_to_use))
    if barcode in config['barcode_label']:
        barcode_label = config['barcode_label'][barcode]
        print("From config file: Barcode {} -> label {}.".format(barcode, barcode_label))
    else:
        # if label not in config, use barcode_parameter directly
        barcode_label = barcode_parameter
        print("Using barcode parameter as label: Barcode {} -> label {}.".format(barcode, barcode_label))
    if '-' in barcode_label:
        # dashes are used to separate and split barcode_labels when >1 so cannot be present in barcode label
        raise ValueError(
            "Label {}: Labels cannot contain a - (dash) character)".format(barcode_label))
    # add to lookup dict
    dict_barcode_parameter = barcode_parameter_from_label.setdefault(
        barcode_label, barcode_parameter)
    # dict_barcode_parameter will be previously set barcode_parameter, if any; check this is not different from current
    if dict_barcode_parameter != barcode_parameter:
        raise ValueError("Label {} encodes multiple barcodes {},{}".format(
            barcode_label, dict_barcode_parameter, barcode_parameter))
    return pandas.Series({'barcode_parameter': barcode_parameter, 'barcode_label': barcode_label})

# find barcode parameter using barcode label
def get_barcode_parameter_from_label(barcode_label):
    return barcode_parameter_from_label[barcode_label]
    # simply uses lookup dict previously generated

# apply f and r barcodes inputid <-> sample mappings
# expect one reverse barcode per file, and unique forward barcodes per file
def apply_barcodes_to_sample_mappings():
    # add barcode_parameter and barcode_label columns to sample_dict
    print("Populating barcode details...")
    for sample_name, sample_details in sample_dict.items():
        sample_details['f_barcode_parameter'],sample_details['f_barcode_label'] = lookup_barcode_from_config(sample_details['f_barcode'], f_barcode_parameter_to_use)
        sample_details['r_barcode_parameter'],sample_details['r_barcode_label'] = lookup_barcode_from_config(sample_details['r_barcode'], r_barcode_parameter_to_use)
    print("Applying barcodes to inputid <-> sample_name mappings...")
    # after running prepare_input.smk, each item in inputid_dict will contain
    # a sample_name key that maps to a list of samples
    # for each input, find sample barcodes using sample_df,
    # check r_barcode and f_barcode
    #     expect one reverse barcode per file, and unique forward barcodes per file
    #     fail if inconsistent reverse barcodes or duplicated forward barcodes
    # add each barcode + barcode_label to inputid_dict,
    for inputid, inputid_details in inputid_dict.items():
        for sample_name in inputid_details['sample_name']:
            sample_f_barcode = sample_dict[sample_name]['f_barcode']
            sample_r_barcode = sample_dict[sample_name]['r_barcode']
            # add/check reverse barcode
            inputid_details.setdefault('r_barcode',sample_r_barcode)
            if inputid_details['r_barcode'] != sample_r_barcode:
                raise ValueError("Inconsistent r_barcode ({}/{}) for input: {}".format(
                    inputid_details['r_barcode'], sample_r_barcode, inputid))
            # add forward barcode
            input_f_barcodes = inputid_details.setdefault('f_barcode', dict())
            input_f_barcodes.setdefault(sample_f_barcode, sample_name)
            if input_f_barcodes[sample_f_barcode] != sample_name:
                raise ValueError("Duplicate f_barcode {} (sample_name {}/{}) for input: {}".format(
                    sample_f_barcode,
                    input_f_barcodes[sample_f_barcode], sample_name,
                    inputid))
            # do the same for f/r barcode_label
            inputid_details.setdefault('r_barcode_label',sample_dict[sample_name]['r_barcode_label'])
            input_f_barcode_labels = inputid_details.setdefault('f_barcode_label', dict())
            input_f_barcode_labels.setdefault(sample_dict[sample_name]['f_barcode_label'], sample_name)

# add sample mappings
apply_barcodes_to_sample_mappings()
print_input_sample_details()

load_config_parameter('invalid_filename') # an invalid filename to force other rules to run

# obtain fasta file for align_target from config file, but throw exception if absent
def get_alignment_target_fasta(align_target):
    if align_target not in config['alignment_target_fasta']:
        raise ValueError(
            "Alignment target {} does not have fasta file specified in config[alignment_target_fasta]".format(align_target))
    return config['alignment_target_fasta'][align_target]

align_target_template = "{align_target}"

# alignment index & library fasta file templates
library_input_templater = path_templater.inputdir().create_fromparts(config['libraries_base_directory'],"","")
library_interm_templater = library_input_templater.intermediatedir()
alignment_index_templater = library_interm_templater.new_template(align_target_template)
generate_library_fasta_from_tsvfile_templater = library_input_templater.new_template('{library_fasta}')
combine_multiple_fasta_templater = library_input_templater.new_template('{combined_library_fasta}')

# expands both ends if inputid has_end2, otherwise just end1
# useful as for this workflow we don't need end2 files, but they
# are processed for stats and fastqc regardless
def end_expander(pathtemplater, wildcards):
    if has_end2(wildcards.inputid): return pathtemplater.expand_ends()
    else: return pathtemplater.end1().use()

# templates for all steps
fastq_stats_inputs_template = input_source_templater.fastq_statsfile()
fastqc_inputs_template = input_source_templater.fastqc_file()

filter_r_index_out_template = input_source_templater.new_directory(config['filter_r_index_output_directory']).new_template("R-{r_barcode_label}___{inputid}").fastq_gzfile()
    # template for a specific r barcode, from an individual input file
filter_r_index_unfiltered_template = filter_r_index_out_template.new_template(
    "noR-{r_barcode_label}___{inputid}")
    # base template for not containing specific r barcode, from an individual input file

filter_r_index_stats_template = filter_r_index_out_template.fastq_statsfile()
filter_r_index_unfiltered_stats_template = filter_r_index_unfiltered_template.fastq_statsfile()


demultiplex_f_index_allbarcodes_template = filter_r_index_out_template.new_directory(config['demultiplex_f_index_output_directory']).new_template("F-{all_f_barcode_labels}_R-{r_barcode_label}___{inputid}").fastq_gzfile()
    # wildcard {all_f_barcode_labels} is concatenation of all barcode labels (separated by '-')
    # base template for all listed f barcodes and a r barcode, from an individual input file

demultiplex_f_index_remaining_template = demultiplex_f_index_allbarcodes_template.new_template("noF-{all_f_barcode_labels}_R-{r_barcode_label}___{inputid}")
    # base template for not containing any of the listed f barcodes, from an individual input file
demultiplex_f_index_out_template = demultiplex_f_index_allbarcodes_template.new_template("F-{f_barcode_label}_R-{r_barcode_label}___{inputid}")
    # base template for specific f and r barcode, from an individual input file
demultiplex_f_index_sample_out_template = demultiplex_f_index_allbarcodes_template.new_template("{sample_name}_F-{f_barcode_label}_R-{r_barcode_label}___{inputid}")
    # base template for specific (implying specific f and r barcode), from an individual input file

demultiplex_f_index_remaining_stats_template = demultiplex_f_index_remaining_template.fastq_statsfile()


demultiplex_f_index_sample_stats_template = demultiplex_f_index_sample_out_template.fastq_statsfile()
demultiplex_f_index_sample_fastqc_template = demultiplex_f_index_sample_stats_template.fastqc_file()

adapter_trim_out_template = demultiplex_f_index_sample_out_template.new_directory(config['adapter_trim_output_directory']).new_affix("_trimmed")

adapter_trim_stats_template = adapter_trim_out_template.fastq_statsfile()
adapter_trim_fastqc_template = adapter_trim_out_template.apply_affix().fastqc_file()

adapter_trim_combined_template = adapter_trim_out_template.apply_format(inputid = "all").outputdir()
    # place combined trimmed files in output, this are useful for some downstream processing applications

align_sam_out_template = path_templater.create_fromparts(config['align_output_directory'],
    "{sample_name}_F-{f_barcode_label}_R-{r_barcode_label}_align-{align_target}___{inputid}",
    ".sam").apply_format(inputid = "all")
    # base template is sample aligned to a target, from an individual input file
    # leaving inputid in this for consistency only, here all is applied to inputid already
align_logfile_template = align_sam_out_template.new_affix("_bowtie2").logfile()
align_sorted_bam_template = align_sam_out_template.outputdir().sorted_bamfile() # NO LONGER USED, can be removed
align_cram_template = align_sam_out_template.outputdir().cramfile() # NO LONGER USED, can be removed
align_sorted_cram_template = align_sam_out_template.outputdir().sorted_cramfile()

library_output_templater = path_templater.outputdir().create_fromparts(config['align_output_directory'],align_target_template,"").fastafile()
    # for copying library FASTA file to alignment output, as CRAM files ideally associated with a reference

sgrna_count_out_template = align_sorted_bam_template.outputdir().new_directory(config['sgrna_count_output_directory']).new_suffix(".grnacount.tsv")

sgrna_count_expand_template = sgrna_count_out_template.new_template("{sample[sample_name]}_F-{sample[f_barcode_label]}_R-{sample[r_barcode_label]}_align-{sample[alignment_target]}___all")

summary_abrije_templater = path_templater.create_fromparts(
    config['summary_abrije_output_directory'], 
    config['summary_abrije_base_filename'],"").outputdir()
analysis_complete_templater = summary_abrije_templater.completefile()
summary_abrije_output_templater = summary_abrije_templater.new_affix("_summary").new_suffix(".tsv")

logdir_templater = path_templater.logdir().create_fromparts("","","")
summary_multiqc_outdir_templater = path_templater.create_fromparts(
    config['summary_multiqc_directory'], "").outputdir()
summary_multiqc_output_templater = summary_multiqc_outdir_templater.new_template("multiqc_report").new_suffix(".html")


# runs full workflow and generates summary
rule all:
    input:
        analysis_complete_templater.use(),
        summary_abrije_output_templater.use(), # comment out to remove summary
        summary_multiqc_output_templater.use()

# executes full workflow
# output can be linked to a run summarisation step
rule run_all:
    input:
        # generate count data files
        sgrna_count_expand_template.expand(
               sample=[value for key, value in sample_dict.items()])
    output:
        touch(analysis_complete_templater.use())

#######
# Rules
#######

# generates an library contents fasta file from a tsv file
# config key 'generate_library_fasta_from_tsvfile' contains
# mapping fasta files to tsv file
# as fasta_filename: tsv_filename
#
# this rule will generate fasta file
# from a tsv file using parse_sgrnalibrary_tsv.py
#
# rule/script parameters given by
# 'generate_library_fasta_from_tsvfile_parameters' config key
# as fasta_filename: parameters
# if missing, no parameters used
#
# if fasta_filename not present in config key, rule will map input
# to invalid_filename, indicating missing files for this rule and giving
# other rules a chance to generate the fasta file
rule generate_library_fasta_from_tsvfile:
    input:
        lambda wildcards: library_input_templater.new_template(
            config['generate_library_fasta_from_tsvfile'].get(
                wildcards.library_fasta, invalid_filename)).use()
                 # default = invalid filename to allow other rules to attempt to generate file
    wildcard_constraints:
        library_fasta='.*[.]fasta$'
    output:
        generate_library_fasta_from_tsvfile_templater.use()
    log:
        generate_library_fasta_from_tsvfile_templater.logfile().use()
    params:
        lambda wildcards:
            config['generate_library_fasta_from_tsvfile_parameters'].get(
                wildcards.library_fasta, '') # default = no parameters
    shell:
        "python scripts/parse_sgrnalibrary_tsv.py "
        "-i {input} "
        "-o {output} "
        "{params} "
        " >{log} 2>&1"


# combines multiple fasta files into an output fasta file
# looks up config 'combine_multiple_fasta' key for an output fasta_filename
# then uses list of values under that key as source files
#
# i.e. 'combine_multiple_fasta' config key is
# mapping fasta files to multiple source fasta files
# output_fasta_filename:
#  - fasta_file1_filename
#  - fasta_file2_filename
#  ...
# output fasta file is generated by cat-ing the source files
# (all/some source files could be generated by
#  generate_library_fasta_from_tsvfile rule)
#
# if output_fasta_filename not present in config key, rule will map input
# to invalid_filename, indicating missing files for this rule and giving
# other rules a chance to generate the fasta file
rule combine_multiple_fasta:
    input:
        lambda wildcards:
            library_input_templater.new_template('{input_fasta}').expand(
                input_fasta = config['combine_multiple_fasta'].get(
                    wildcards.combined_library_fasta,
                    [invalid_filename]))
                    # default = invalid filename to allow other rules to attempt to generate file
    wildcard_constraints:
        combined_library_fasta='.*[.]fasta$'
    output:
        combine_multiple_fasta_templater.use()
    shell:
        "cat "
        "{input} "
        "> {output} "

# builds a bowtie2 index for an alignment_target from a fasta file
# looks up config 'alignment_target_fasta' key for
# mapping alignment_target to a fasta file
# as alignment_target_name : fasta_filename
#
# mapped fasta files can be generated with 'generate_library_fasta_from_tsvfile'
# and 'combine_multiple_fasta' below
#
# if alignment_target_name is missing in config key,
# an exception will be throw
rule generate_alignment_index:
    input:
        lambda wildcards: library_input_templater.new_template(
            get_alignment_target_fasta(wildcards.align_target)).use()
    output:
        touch(alignment_index_templater.use())
    log:
        alignment_index_templater.logfile().use()
    conda:
        "envs/bowtie2.yaml"
    threads: config['generate_alignment_index_use_threads']
    shell:
        "bowtie2-build --threads {threads} -f "
        "{config[generate_alignment_index_additional_parameters]} "
        "{input} "
        "{output} "
        " >{log} 2>&1"


# generic rule to generate stats on reads and bases in a fastqc.gz file
# v1.1.dev10: added error logging, updated to cache stderr to a temp file,
# and then append contents to output; no error should be identical to previous
rule generic_fastq_stats:
    input:
        basic_fastq_gz_templater.use()
    output:
        stats = basic_fastq_gz_templater.fastq_statsfile().use(),
        log = temp(basic_fastq_gz_templater.fastq_statsfile().new_affix("_stats").logfile().use())
    threads: config['generic_fastq_stats_consume_threads']
    shell:
        """
        scripts/summarise_fq.sh {input} >{output.stats} 2>{output.log}
        cat {output.log} >>{output.stats}
        """

# as above, but input file is expected to be within output top dir
# should only be used by snakemake if input cannot be found above
rule generic_fastq_stats_output:
    input:
        basic_fastq_gz_templater.outputdir().use()
    output:
        stats = basic_fastq_gz_templater.fastq_statsfile().use(),
        log = temp(basic_fastq_gz_templater.fastq_statsfile().new_affix("_stats").logfile().use())
    threads: config['generic_fastq_stats_consume_threads']
    shell:
        """
        scripts/summarise_fq.sh {input} >{output.stats} 2>{output.log}
        cat {output.log} >>{output.stats}
        """

# generic rule to run fastqc on a fastqc.gz file
rule generic_fastqc:
    input:
        basic_fastq_gz_templater.use()
    output:
        basic_fastqc_templater.use()
    log:
        basic_fastqc_templater.logfile().use()
    params:
        output_dir = basic_fastqc_templater.get_directory()
    conda:
        "envs/fastqc.yaml"
    threads: config['generic_fastqc_consume_threads']
    shell:
        "fastqc -o {params.output_dir} {input} "
        "{config[generic_fastqc_additional_parameters]} "
        ">{log} 2>&1"


# chain to this rule to get stats on inputs
rule fastq_stats_inputs:
    input:
        lambda wildcards: end_expander(fastq_stats_inputs_template, wildcards)
    output:
        touch(fastq_stats_inputs_template.end1_2().completefile().use())

# chain to perform fastqc on input reads
rule fastqc_inputs:
    input:
        lambda wildcards: end_expander(fastqc_inputs_template, wildcards)
    output:
        touch(fastqc_inputs_template.end1_2().completefile().use())


# filter reads by index in reverse read
rule filter_r_index:
    input:
        input_source_templater.end1().use(),
        rules.fastq_stats_inputs.output,  # chain to run
        rules.fastqc_inputs.output
    output:
        filtered_reads = filter_r_index_out_template.use(),
        unfiltered_reads = filter_r_index_unfiltered_template.use()
    log:
        filter_r_index_out_template.logfile().use()
    conda:
        "envs/filter_illumina_index.yaml"
    params:
        r_barcode_parameter = lambda wildcards: get_barcode_parameter_from_label(
            wildcards.r_barcode_label)
    threads: config['filter_r_index_use_threads']
    shell:
        "filter_illumina_index {input[0]} "
        "--index {params.r_barcode_parameter} "
        "--mismatches {config[filter_r_index_mismatches_allowed]} "
        "--compresslevel {config[filter_r_index_compression_level]} "
        "--threads {threads} "
        "--filtered {output.filtered_reads} "
        "--unfiltered {output.unfiltered_reads} "
        "{config[filter_r_index_additional_parameters]} "
        " >{log} 2>&1"

# chain to this rule to get stats of filter_r_index files
rule filter_r_index_stats:
    input:
        filtered_reads_stats = filter_r_index_stats_template.use(),
        unfiltered_reads_stats = filter_r_index_unfiltered_stats_template.use()
    output:
        touch(filter_r_index_out_template.completefile().use())

# demultiplex with cutadapt
# note, will output demultiplexed reads but Snakemake doesn't support variable output filenames
# therefore, indicate that only output file for non-demultiplexed (untrimmed) reads produced
# additional rule 'produces' output files for downstream use by renaming the files produced here
#
# note2: if there are no untrimmed reads, then the non-demultiplexed file will not be produced
# causing this rule to fail. while this is very unlikely to happen with raw real-world data,
# it could happen if the reads have already been demultiplexed prior to running this script,
# which is a potential use case. to solve this issue, touch has been added to output
# to ensure that output is always created. a side effect is that this does mean that the rule
# will always succeed; for cases where the rule has actually failed, this should be caught
# by a failure return code
#
# note3: convert touch to creating a valid gzip file, as otherwise downstream processing of
# untrimmed file may fail
rule demultiplex_f_index:
    input:
        # tell counts to run
        rules.filter_r_index_stats.output,
        **rules.filter_r_index.output,
    output:
        not_f_index_demultiplex = demultiplex_f_index_remaining_template.use()
    log:
        demultiplex_f_index_allbarcodes_template.logfile().use()
    conda:
        "envs/cutadapt.yaml"
    params:
        f_barcode_parameters_options = lambda wildcards: " ".join(
            "-g {}={}".format(barcode_label, get_barcode_parameter_from_label(barcode_label)) for barcode_label in wildcards.all_f_barcode_labels.split("-")),
            # generate -g label1=barcode1 -g label2=barcode2 ...
            # relies on '-' to split barcode_labels
        f_barcode_all_outputs = lambda wildcards: " ".join(
            demultiplex_f_index_out_template.format(
                f_barcode_label=barcode_label, **wildcards) for barcode_label in wildcards.all_f_barcode_labels.split("-")),
            # generate filenames for each f_barcode_label
        generic_filename = lambda wildcards: demultiplex_f_index_out_template.format(
            f_barcode_label="{name}",**wildcards)
            # return generic filename for cutadapt containing {name} placeholder
            # lambda needed to 'escape' braces
    threads: config["demultiplex_f_index_consume_threads"]
    shell:
        # cutadapt (< v3.0) does not produce files if no reads are copied to an output
        # to prevent subsequent failures due to file not found,
        # pregenerate empty files so a file is always present
        'gzip < /dev/null > {output} 2>{log} && '
            # creates an empty (0-sized, no newline) (valid) gzip file for noF
        'cat {output} | tee {params.f_barcode_all_outputs} > /dev/null 2>>{log} && '
            # copies empty file to multiple outputs, one for each F-
            # then run cutadapt...
        "cutadapt "
        #"--cores {threads} " # not supported when demultiplexing
        "{params.f_barcode_parameters_options} "
        "--overlap={config[demultiplex_f_index_overlap]} "
        "--error-rate={config[demultiplex_f_index_error_rate]} "
        "-o {params.generic_filename} "
        "--untrimmed-output {output.not_f_index_demultiplex} "
        "{config[demultiplex_f_index_additional_parameters]} "
        "{input.filtered_reads} "
        " >>{log} 2>&1"
        # example call:
        # cutadapt -a one=TATA -a two=GCGC -o trimmed-{name}.fastq.gz input.fastq.gz
        # cutadapt -g file:barcodes.fasta --no-trim --untrimmed-o untrimmed.fastq.gz -o trimmed-{name}.fastq.gz input.fastq.gz

# this rule 'produces' demultiplexed files for each sample
# actual work is done by demultiplex_f_index, which actually produces
# actual data files for each forward barcode
# this rule simply makes a symlink to these output files (adding sample_name) for
# downstream use
#
# input is non-demultiplexed (remaining) reads file, which is produced at
# same time when demultiplexing all f_barcodes
rule stage_demultiplex_f_index:
    input:
        # expect non-demultiplex stats as input, this ensures the non-demultiplexed reads are also counted
        not_demultiplex_f_index_stats = lambda wildcards:
            demultiplex_f_index_remaining_stats_template.expand(
                all_f_barcode_labels='-'.join(inputid_dict[wildcards.inputid]['f_barcode_label'].keys()),
                **wildcards) # need to lookup all f_barcode labels for this inputid to select correct file
    log:
        demultiplex_f_index_sample_out_template.logfile().use()
    output:
        f_index_demultiplex = demultiplex_f_index_sample_out_template.use()
    params:
        input = demultiplex_f_index_out_template.use()
        # name of actual input file to rename, obviously cannot be specified in input
        # as no rule specifies it creates this file as output
    shell:
        # head is used to failfast if params.input does not exist
        "head {params.input} -c 0 && ln -Tsrv {params.input} {output} "
        ">{log} 2>&1"

# perform adapter trimming, accepting only sequences of 20bp after trimming
# details of sequenced PCR product: (^ indicates 5' and 3' adapters)
# 5' AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCTTAAGTAGAGtcttgtggaaaggacgaaacaccg
#                                                              SBBBBBBBB^^^^^^^^^^^^^^^^^^^^^^^^
# = PCR2-F01 primer, S=Stagger, B=Barcode
#
# gagctggacggcgacgtaaa
# =sgRNA (e.g. EGFP)
# gttttagagctagaaatagcaagttaaaataaggctagtccgttatcaacttgaaaaagtggcaccgagtcggtgcttt
# ^^^^^^^^^^^^^^^^^^^^^^^^
# tttaagcttggcgtaactagatcttgagacaaatggcagtattcatccacaattttaaaagaaaaggggggattggggggt
# = vector sequence
#
# acagtgcaggggaaagaatagtagaAAGATCGGAAGAGCACACGTCTGAACTCCAGTCACCTCTACTTATCTCGTATGCCGTCTTCTGCTTG
# tgtcacgtcccctttcttatcatctTTCTAGCCTTCTCGTGTGCAGACTTGAGGTCAGTGGAGATGAATAGAGCATACGGCAGAAGACGAAC 5'
#                          S                                  BBBBBBBB
# = PCR2-R01 primer, S=Stagger, B=Barcode
# adapter: -g TCTTGTGGAAAGGACGAAACACCG...GTTTTAGAGCTAGAAATAGCAAGT
# stored in config file
rule adapter_trim:
    input:
        demultiplex_f_index_sample_stats_template.use(), # chain to run
        demultiplex_f_index_sample_fastqc_template.use(),
        **rules.stage_demultiplex_f_index.output,
    output:
        trimmed_reads = adapter_trim_out_template.use()
    log:
        adapter_trim_out_template.logfile().use()
    conda:
        "envs/cutadapt.yaml"
    threads: config['adapter_trim_use_threads']
    shell:
        "cutadapt {config[adapter_trim_adapter]} "  # use linked adaptors
        "--cores={threads} "
        "{config[adapter_trim_parameters]} "
        "--quality-cutoff={config[adapter_trim_quality_cutoff]} "
        "--error-rate={config[adapter_trim_error_rate]} "
        "--max-n={config[adapter_trim_max_n]} "
        "--minimum-length={config[adapter_trim_minimum_length]} "
        "--maximum-length={config[adapter_trim_maximum_length]} "
        "{config[adapter_trim_additional_parameters]} "
        "-o {output} "
        "{input.f_index_demultiplex} "
        ">{log} 2>&1"

# chain to this rule to produce stats for all trimmed input files for a sample
rule trimmed_stats:
    input:
        expand_all_inputids_function_factory(adapter_trim_stats_template),
        expand_all_inputids_function_factory(adapter_trim_fastqc_template)
    output:
        touch(adapter_trim_out_template.new_suffix("+.stats").completefile().pformat(inputid = "all"))

# combine all trimmed data files into one file
rule combine_trimmed:
    input:
        expand_all_inputids_function_factory(adapter_trim_out_template)
    output:
        trimmed_reads = adapter_trim_combined_template.use()
    log:
        adapter_trim_combined_template.logfile().use()
    shell:
        "cat {input}  > {output} "
        "2>{log}"

# Previous: align using bwa (aln/samse)
# Update: align using bowtie2 with defined penalties
rule align:
    input:
        rules.trimmed_stats.output, # chain to run
        adapter_trim_combined_template.fastq_statsfile().use(),
        **rules.combine_trimmed.output,
        sgrna_index = alignment_index_templater.use()
    output:
        sam = pipe(align_sam_out_template.use())
    log:
        align_logfile_template.use()
    threads: max(1,workflow.cores - int(workflow.cores * (config["pipe_align_postprocess_cores_proportion"])))
    conda:
        "envs/bowtie2.yaml"
    shell:
        "bowtie2 "
        "--ma={config[align_match_bonus]} "
        "--mp={config[align_mismatch_penalty]} "
        "--rdg={config[align_read_gap_penalty]} "
        "--rfg={config[align_reference_gap_penalty]} "
        "--np={config[align_n_penalty]} "
        "--score-min={config[align_score_min_func]} "
        "--n-ceil={config[align_nceil_func]} "
        "{config[align_modes]} "
        "{config[align_sensitivity_settings]} "
        "{config[align_additional_parameters]} "
        "-x {input.sgrna_index} "
        "-U {input.trimmed_reads} "
        "--threads {threads} "
        " 2>{log} > {output}"


# copy library fasta to output, in order to be associated with CRAM alignments
rule copy_fasta_to_output:
    input:
        lambda wildcards: library_input_templater.new_template(
            get_alignment_target_fasta(wildcards.align_target)).use()
    output:
        library_output_templater.use()
    shell:
        "cp {input} {output}"


# index a FASTA file
# generic rule using templated top dir
rule fasta_index:
    input:
        toptemplate_basic_fasta_templater.use()
    output:
        toptemplate_basic_fasta_templater.faifile().use()
    conda:
        "envs/samtools.yaml"
    shell:
        "samtools faidx {config[fasta_index_additional_parameters]} {input}"


# NO LONGER USED, can be removed
# linked by pipe to 'align' rule to postprocess align output
# use samtools to created (unsorted) cram file
# requires config options: sam_to_cram_convert_parameters
#                          sam_to_cram_additional_parameters
rule sam_to_cram:
    input:
        sam = align_sam_out_template.use(),
        reference = library_output_templater.use(),
        reference_index = library_output_templater.faifile().use(),
    output:
        align_cram_template.use()
    log:
        align_cram_template.new_affix("_tocram").logfile().use()
    conda:
        "envs/samtools.yaml"
    threads: max(1,int(workflow.cores * (config["pipe_align_postprocess_cores_proportion"])))
    shell:
        "samtools view -@ {threads} -o {output} "
        "--reference={input.reference} "
        "{config[sam_to_cram_convert_parameters]} "
        "{config[sam_to_cram_additional_parameters]} "
        " {input.sam} "
        " >{log} 2>&1"

# linked by pipe to 'align' rule to postprocess align output
# use samtools sort to created sorted cram file
# also include fasta (and fai index) in output dir as reference file for CRAM
# these may be unneeded if reference-less CRAM is produced, but useful
# for archiving
rule sam_to_sorted_cram:
    input:
        sam = align_sam_out_template.use(),
        reference = library_output_templater.use(),
        reference_index = library_output_templater.faifile().use(),
    output:
        align_sorted_cram_template.use()
    log:
        align_sorted_cram_template.new_affix("_sort").logfile().use()
    conda:
        "envs/samtools.yaml"
    threads: max(1,int(workflow.cores * (config["pipe_align_postprocess_cores_proportion"])))
    shell:
        "samtools sort -@ {threads} -o {output} "
        "--reference={input.reference} "
        "{config[sam_to_sorted_cram_convert_parameters]} "
        "{config[sam_to_sorted_cram_additional_parameters]} "
        "{input.sam} "
        " >{log} 2>&1"


# NO LONGER USED, can be removed
# linked by pipe to 'align' rule
# use samtools to sort sam file, will automatically output to bam
# based on extension
rule sam_to_sorted_bam:
    input:
        align_sam_out_template.use()
    output:
        align_sorted_bam_template.use()
    log:
        align_sorted_bam_template.new_affix("_sort").logfile().use()
    conda:
        "envs/samtools.yaml"
    threads: max(1,int(workflow.cores * (config["pipe_align_postprocess_cores_proportion"])))
    shell:
        "samtools sort -@ {threads} -o {output} {input}"
        " >{log} 2>&1"


# NO LONGER USED, can be removed
# sort bam file
# generic rule
rule bam_sort:
    input:
        basic_bam_templater.use()
    output:
        basic_bam_templater.sorted_bamfile().use()
    log:
        basic_bam_templater.new_affix("_sort").logfile().use()
    conda:
        "envs/samtools.yaml"
    threads: 12
    shell:
        "samtools sort -@ {threads} -o {output} {input}"
        " >{log} 2>&1"

# NO LONGER USED, can be removed
# index a sorted bam file
# generic rule
rule bam_index:
    input:
        basic_bam_templater.use()
    output:
        basic_bam_templater.baifile().use()
    conda:
        "envs/samtools.yaml"
    threads: 12
    shell:
        "samtools index -@ {threads} -b {input} {output}"


# Parse alignment file to produce counts for each sgRNA, output as TSV
rule sgrna_count:
    input:
        align_sorted_cram_template.use()
    output:
        sgrna_count_out_template.use()
    log:
        sgrna_count_out_template.logfile().use()
    conda:
        "envs/count_fgs_sam.yaml"
    threads: config['sgrna_count_consume_threads']
    shell:
        "count_fgs_sam {input} "
        "--outputfile {output} "
        "--perfectscore {config[sgrna_count_perfectscore]} "
        "--expectedlength {config[sgrna_count_expectedlength]} "
        "--unambiguousdelta {config[sgrna_count_unambiguousdelta]} "
        "--acceptablescore {config[sgrna_count_acceptablescore]} "
        "--acceptableminlength {config[sgrna_count_acceptableminlength]} "
        "--acceptablemaxlength {config[sgrna_count_acceptablemaxlength]} "
        "{config[sgrna_count_output_parameters]} "
        "{config[sgrna_count_additional_parameters]} "
        " >{log} 2>&1"

# for debugging
if verbose: print_all_rules()

# summarisations
import subprocess
import sys
import os
import shlex

# parser for bowtie2 summary
bowtie2_parse_params = {
    "parse_regex_reads" : "[ ]*(?P<value>[0-9.]+).*(?P<heading>(reads|unpaired|aligned 0 times|aligned exactly 1 time)).*",
    "parse_regex_percent" : "[ 0-9]*[(](?P<value>[0-9.]+(?P<heading2>%))[)][ ]+(?P<heading1>(unpaired|aligned 0 times|aligned exactly 1 time)).*",
    "parse_regex_overall" : "[ ]*(?P<value>[0-9.]+(?P<heading2>%))[ ]+(?P<heading1>(overall alignment rate)).*"
}

# parser for count_fgs_sam summary
count_fgs_sam_parse_params = {
    "parse_regex_reference" : "[ ]*(?P<heading0_Reference_>)(?P<heading1>(Number of alignment references|Not of expected length)[^:]*):[ ]*(?P<value>[0-9]+)",
    "parse_regex_aligned" : "[ =+]*(?P<heading0_Aligned_>)(?P<heading1>(Entries|Processed|Ignored)[^:]*):[ ]*(?P<value>[0-9]+)",
    "parse_regex_aligned2" : "[ ]*(?P<heading0_Aligned_>)(?P<heading1>(Mapped entries|With alternative scores)[^:]*):[ ]*(?P<value>[0-9]+)",
    "parse_regex_aligned3" : "[ ]*(?P<heading0_AlignedFlags_>)(?P<heading1>(Paired reads|Mapped to reverse complement)[^:]*):[ ]*(?P<value>[0-9]+)",
    "parse_regex_counts" : "[ ]*(?P<heading0_Counts_>)(?P<heading1>(Total)[^:]*):[ ]*(?P<value>[0-9]+)"
}

logsets_path_type_list = [
    (fastq_stats_inputs_template.use(), "tsv_wide",
        {"label1_sample_name" : "*", "label2_f_barcode_label" : "*",
        "label4_r_barcode_label" : "*"}),
        # for nicer summary output, override `label4_r_barcode_label`
        # on all logs prior to F demultiplex (including F unfiltered)
        # Also, override label1_sample_name and label2_f_barcode_label to *
    (filter_r_index_stats_template.use(), "tsv_wide",
        {"label1_sample_name" : "*", "label2_f_barcode_label" : "*",
        "label4_r_barcode_label" : "*",
        "{r_barcode_label}": "comment_r_barcode_label"}),
        # store r_barcode_label as comment instead
    (filter_r_index_unfiltered_stats_template.use(), "tsv_wide",
        {"label1_sample_name" : "*", "label2_f_barcode_label" : "*",
        "label4_r_barcode_label" : "*",
        "{r_barcode_label}": "comment_r_barcode_label",
        "heading_prefix" : "missing_R_barcode_"}),
    (demultiplex_f_index_sample_stats_template.use(), "tsv_wide"),
    (demultiplex_f_index_remaining_stats_template.use(), "tsv_wide",
        {"label1_sample_name" : "*", "label2_f_barcode_label" : "*",
        "label4_r_barcode_label" : "*",
        "{r_barcode_label}": "comment_r_barcode_label",
        "heading_prefix" : "missing_F_barcode_"}),
    (adapter_trim_stats_template.use(), "tsv_wide"),
    (align_logfile_template.use(), "regex", 
        {**bowtie2_parse_params, "label3_inputid" : "all"}),
        # {inputid} is not kept, so manually readd
    (sgrna_count_out_template.logfile().use(), "regex", 
        {**count_fgs_sam_parse_params, "label3_inputid" : "all"})
]

def generate_logsets(logsets_path_type_list):
    # generate full logset list, using step name as logset_name
    ret = []
    existing_logsets = set()
    logset_name_re = re.compile("[^/]+/(?P<stepname>[^/]+)/(?P<unfiltered_name>no[FR]|).*")
    for logset_path, *logset_type_params in logsets_path_type_list:
        logset_name = logset_name_re.match(logset_path).group("stepname")
        while logset_name in existing_logsets: # ensures we don't replace a logset
            logset_name=logset_name+"_"
        existing_logsets.add(logset_name)
        ret.append((logset_name, logset_path, *logset_type_params))
    return ret
        

abrije_config = {
        "defaults" : {
            "{sample_name}" : "label1_sample_name",
            "{f_barcode_label}": "label2_f_barcode_label",
            "{inputid}" : "label3_inputid",
            "{r_barcode_label}": "label4_r_barcode_label",
            "{end_label}": "tag_end_label",
            "{align_target}": "comment_align_target",
            "{all_f_barcode_labels}": "comment_which_F_barcodes_missing"
        }
    } 


process_log_config_manual = {
    "log_sets" : generate_logsets(logsets_path_type_list),
    "config" : abrije_config
}


# summarise all log files using abrije
rule summarise_logs_abrije:
    input:
        rules.run_all.output
    output:
        summary_abrije_output_templater.use()
    log:
        summary_abrije_output_templater.logfile().use()
    params:
        json_config = lambda wildcards: shlex.quote(json.dumps(process_log_config_manual))
    conda:
        "envs/abrije.yaml"
    shell:
        "abrije "
        "-j {params.json_config} "
        "-o {output} "
        "{config[summary_abrije_additional_parameters]} "
        " >{log} 2>&1"

# summarise all log files using abrije
rule summarise_logs_multiqc:
    input:
        rules.run_all.output
    output:
        summary_multiqc_output_templater.use()
    log:
        summary_multiqc_output_templater.logfile().use()
    params:
        output_dir = summary_multiqc_outdir_templater.use(),
        log_dir = logdir_templater.use()
    conda:
        "envs/multiqc.yaml"
    shell:
        "multiqc {params.log_dir} "
        "-c {config[summary_multiqc_config_path]} "
        "--outdir {params.output_dir} "
        "-f "
        "{config[summary_multiqc_additional_parameters]} "
        " >{log} 2>&1"
