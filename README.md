# pooled_screen_counts

## Processing of whole-genome pooled CRISPR/Cas9 screen sequencing data

This Snakemake workflow is designed to process Illumina sgRNA sequencing data
for a whole-genome pooled CRISPR/Cas9 screen. The input data are FASTQ files
already demultiplexed on the reverse (i7) index by the Illumina software, with
the index placed at the `sample number` position at the end of the sequence
identifiers in the FASTQ files. Only the forward read (R1) is used by this
workflow. The user supplies a sample list in CSV format, which specifies
sample names, barcodes (indexes) and alignment target (library sgRNA sequences).

The workflow is supplied with a default configuration file
(`default_config.yaml`) containing default parameters. An additional
configuration file (`override_config.yaml`) contains parameters that override
these defaults. To change parameters, overrides should be added to the
override config file (`override_config.yaml`).

It will do the following steps:

1) Filter reads based on a reverse barcode (index). (This should be unnecessary,
as the reads should already be demultiplexed by the Illumina software, but is
a useful check to ensure everything is as expected.)

2) Demultiplex reads based on forward barcode (index).

3) Performs fastqc on demultiplexed data.

4) Trims adapter sequences flanking sgRNA to obtain only sgRNA.

5) Generates an bowtie2 alignment index for the alignment target.

6) Aligns data to alignment target with bowtie2.

7) Combines data for each sample from multiple files (if any).

8) Produces final count of each sgRNA per sample.

### Terminology

#### `alignment_target` / `align_target`

This is the name of the target sgRNA library to align to. In the config file,
the path to a FASTA file for each `alignment_target` must be added under the
`alignment_target_fasta` key. The workflow will align (with Bowtie 2) the sequenced
sgRNAs to those in this FASTA, and ultimately produce a TSV file of counts.

New `alignment target` can be created by simply generating a FASTA file
containing sgRNA sequences to align to, and adding the path of this FASTA file
to the config file under `alignment_target_fasta`.

#### `f_barcode` / `f_barcode_label` / `f_barcode_parameter`

Forward (i5) barcode (index) present in PCR primer. In the sample list, a named
`f_barcode` is specified (e.g. F01). This is looked-up in the config file to
give a `f_barcode_label`, which is the label added to any files processed for
this barcode (`F-{f_barcode_label}`). The default label is the 8-bp sequence of
the barcode.

The `f_barcode_parameter` for this barcode
is also looked up from the config file; this is the adapter parameter supplied
to cutadapt when demultiplexing. Different barcode parameters can be used by
changing the `f_barcode_parameter_to_use` in the config file, e.g. the default
of `f_barcode_parameter_to_use: noninternal` will tell the workflow to read
`f_barcode_parameter` from the `f_barcode_noninternal` key in the config file,
which is a non-internal adapter sequence for the barcode. This could be changed
to `sequence` to use the 8-bp sequence of the barcode directly as an adapter,
or `anchored` to use a 5' anchored adapter. The default of `noninternal` has
the advantage that it ensures that adapter must be found at the 5' end of the
read (unlike `sequence`, which could be found internally) while allowing some
missing nucleotides before the index without these  being counted as errors
(unlike `anchored`).

If desired, barcode labels or parameters can be modified in the config file,
or new barcodes can be added here.

#### `r_barcode` / `r_barcode_label` / `r_barcode_parameter`

As above, but for the reverse (i5) barcode (index). The default
`r_barcode_parameter_to_use` is `sequence`,as filtering the reverse barcodes
is based on this sequence in the sequence identifiers of the FASTQ file.

#### `input_regex`

This is a regular expression specified for each sample that is used to find
input FASTQ files for the sample. All FASTQ file paths that contain this regular
expression (tested with Python `re.search`) are used as input files for the
sample (reads will then be filtered by `r_barcode` and demultiplexed on
`f_barcode`). As the workflow expects reads to be already demultiplexed on the
reverse barcode, the `input_regex` might contain, for example, the name of the
reverse index files (`R01-data`).

### Input files

#### Illumina FASTQ files

Should already demultiplexed on reverse barcode, i.e. separate file for each
reverse barcode. Multiple FASTQ files per sequence allowed. Only forward read
(R1) used. Should be placed in `in_sequencing_data` directory, with names of top
level subdirectories used as sequencing `run`-name, and FASTQ filename (without
extension) used as `filename`; i.e. `in_sequencing_data/{run}/**/{filename}.fastq.gz`
naming convention. All processed files are collapsed into a single directory
per step and identified as `{run__filename}`.

FASTQ files are matched to samples by the use of `input_regex`, any FASTQ file
path that contains the `input_regex` for the sample are used as input data
for the sample.

#### Input FASTQ files that do not need to be filtered, (forward) demultiplexed or trimmed

An initial step in this workflow is to filter the reads by the reverse index or
barcode that is expected to be present in the `<sample number>` field of the
FASTQ read sequence identifier of all reads. If this barcode is not present in
that field, this step can be skipped using the `noneR` barcode, which causes all
reads to be passed through by the filtering step. (There is no support in this
workflow to demultiplex by reverse barcode itself).

If the input FASTQ files have already been demultiplexed (i.e. one FASTQ file
per sample), this workflow can be customised to prevent further demultiplexing.
Use the `noneF` barcode and look in `default_config.yaml` for example
alternative parameters for pre-demultiplexed data and add this to your
`override_config.yaml`. You may also need to change the `input_end1_suffix` and
`ignore_missing_end2` to allow correct matching of the input files, but examples
of these are also given in the comments of the `default_config.yaml` file.

Similarly, if input FASTQ files have already been demultiplexed as well as
trimmed down to the sgRNA sequences themselves, example alternative parameters
are shown in  `default_config.yaml` for this configuration.


#### Library FASTA files (alignment target)

Simple FASTA file of sequence identifiers and sgRNA sequences to align to. The
output sgRNA counts will give a count for each sequence identifier in the
FASTA file. All sequences in the FASTA file should be _unique_, otherwise
the Bowtie 2 alignment will randomly assign reads to one of the redundant sequences.
(Note that the GeCKO v2 library contains identical sgRNAs for different targets
so the input FASTA file should be processed to deal with these).

The script `scripts/generate_brunello_fasta.sh` generates a FASTA file for
the Brunello library based on the `broadgpp-brunello-library-contents.txt` file
from Addgene. It uses the `scripts/parse_brunello.py` script, and also generates
a variation of this library contaning two additional sgRNAs used as PCR positive
controls in our amplification of the sgRNA casettes for sequencing.

#### Sample list
An example `sample_list.csv` is as follows (identical to
`example_sample_list.csv`):

    sample_name,f_barcode,r_barcode,input_regex,alignment_target
    sample1,F01,R01,R01_data,GeCKOv2
    sample2,F02,R01,R01_data,Brunello

These are the required columns; additional columns can be added by the user to
store any additional information about the samples.

### Output files

#### sgRNA counts

These are placed in the `out5_count` directory and generated with
`count_fgs_sam`. The result is a TSV file consisting of reference sequence
(sgRNA) identifier and number of mapped reads for that sequence in different
count buckets depending on the settings used.

As noted in [`samtools idxstats`](http://www.htslib.org/doc/samtools.html): “The output is TAB-delimited with each line consisting of reference sequence name, sequence length, # mapped reads and # unmapped reads.”

The naming convention for output files is:
`{sample_name}_F-{f_barcode_label}_R-{r_barcode_label}_align-{align_target}___all.count.tsv`

#### Intermediate files and logs

Various intermediate files and logs are produced by the workflow, including
`r_barcode`-filtered reads, `f_barcode`-demultiplexed reads (and remaining
non-demultiplexed sequences), FASTQC files after demultiplexing, read and bp
counts for each FASTQ file etc.

The naming convention for these files is generally
`{sample_name}_F-{f_barcode_label}_R-{r_barcode_label}_align-{align_target}___{datafile_run}__{datafile_basename}`
although various parts of this name will be missing at different steps. `noF-{f_barcode_label1}-{f_barcode_label2}...` contains non-demultiplexed reads for
the `f_barcode`.

### Conda and multi-core support

The workflow is designed to use conda, and conda environments are provided
for each step, so the `--use-conda` parameter should be used in for Snakemake.
It is also designed to use multiple cores. Therefore, the `--cores x` option
can be used, e.g. an invocation command might be:
`snakemake --use-conda --cores 6`

This version of the workflow uses a pipe command. Due to a snakemake issue,
this means that at least 2 cores must be used: one for the feeder command,
one for the pipe command. The cores are distributed between the feeder
and pipe command at a 2:1 ratio.

### Additional details

-   Author:       Tet Woo Lee, based on GeckoDemultiplexeR by Peter Tsai
-   Copyright:    © 2018-2020 Tet Woo Lee
-   Licence:      GPLv3

### Change log

version 1.1r10 2024-07-08
: Move `prepare_input` to submodule.
: Update `prepare_input` to v1.0.0.dev13 2024-05-19.

version 1.1r9 2022-10-20
: Move `--trimmed-only` to config for `rule adapter_trim:`.
: Document parameters for trimming to fixed length.

version 1.1r8 2022-09-07
: Force overwrite MultiQC report to ensure filenames consistent with workflow.

version 1.1r7 2022-09-05
: Add FastQC to trimmed reads.

version 1.1r6 2022-09-05
: Ensure all output files are created at `demultiplex_f_index` step; using 
  manual bash command for this; cutadapt v3.0+ will do this by default, but 
  stick with cutadapt v1.18 for compatibility.

version 1.1r5 2022-07-02
: Add `abrije` log summary.
: Add `multiqc` log summary.

version 1.1r4 2020-06-29
: Substitute `rule rename_demultiplex_f_index` with `rule stage_demultiplex_f_index`
  to ensure original output files of `demultiplex_f_index` remain present after
  execution of the rule.

version 1.1r3 2020-04-11
: Modify default options for `rule filter_r_index`.

version 1.1r2 2020-04-11
: Add `pigz` to `envs/filter_illumina_index.yaml`.

version 1.1 2020-04-11
: Bump `filter_r_index` to `1.0.4` for speed up.

version 1.1.dev13 2020-04-01
: Bump `filter_r_index` to `1.0.3.post2` to improve output of this step.

version 1.1.dev12 2020-04-01
: Added example parameters for data that is already demultiplexed or trimmed.
: Update `filter_r_index` rule to use new version of `filter_illumina_index`
  that supports passthrough.
: Added `noneR` reverse barcode to passthrough reads that do not have a
  barcode in the read sequence identifier.
: Added `noneR` forward barcode to support that that is already demultiplexed.
: Rename `_nonrc` barcdoes to `_rc`.
: Added `additional_parameters`, `use_threads` and `consume_threads` parameters
  to most rules.

version 1.1.dev11 2020-03-31
: Convert `touch()` in `demultiplex_f_index` to proper gz file creation
: Add concatenation of `summarise_fq.sh` errors to output file
: Update fastqc to latest version for no-error handling of empty files

version 1.1.dev10 2020-03-31
: Added `touch()` to `demultiplex_f_index` to allow rule to work even if
  no untrimmed reads present in input.
: Update `summarise_fg.sh` to initialised to 0 counts, to give consistent
  output for empty files.

version 1.1.dev9 2020-03-29
: Switch to using sorted CRAM files for alignment

version 1.1.dev8 2020-03-29
: Switch to using CRAM files for alignment

version 1.1.dev7 2020-03-29
: Remove post-trim filter as no longer required with count_fgs_sam

version 1.1.dev6 2020-03-29
: Change align pipe to samtools sort and output to bam
: Switch to count_fgs_sam for counting sgRNAs

version 1.1.dev5 2020-03-11
: Add post-trim filter to filter reads by length before alignment.

version 1.1.dev4 2020-03-11
: Switch to 'local' alignment mode with Bowtie2, with plan to filter out
  ambiguous alignments in next version.

version 1.1.dev3 2020-03-10
: Add quality trim option to adapter trim.
: Change default adapter trim options to be more lenient; rely on alignment
  options to control extent of mismatches to library allowed.
: Default alignment options allow <=1 good-quality mismatches;
  <=2 poor-quality mismatches; <=1 bp gap, and max 2 Ns.

version 1.1.dev2 2020-03-10
: Alter core distribution calculation with sam_to_bam pipe

version 1.1.dev1 2020-03-10
: Changed BWA alignment to Bowtie 2 to allow more deterministic output with
  imperfect alignments (based on Bowtie 2 mismatch/gap penalties)

version 1.0.dev1 2019-01-03
: First complete, working version
